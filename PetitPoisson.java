public class PetitPoisson{

    private int posX;
    private int posY;
    private int vitesse;

    public PetitPoisson(int posX, int posY, int vitesse){
        this.posX = posX;
        this.posY = posY;
        this.vitesse = vitesse;
    }

    public void nage(){
        this.posX += vitesse;
    }

    public Dessin getDessin(){
        Dessin dessin = new Dessin();
        String couleur = "0x0000F0";
        dessin.ajouteChaine((int)this.posX, (int)this.posY, "><>", couleur);
        return dessin;
    }

    public boolean horsCadre(){
        boolean dehors = false;
	if (this.posx > 120 || this.posy < 50){
            dehors = true;
        }
        if (this.posx < 0 || this.posy > 50){
            dehors = true;
        }
        return dehors;
    }
}