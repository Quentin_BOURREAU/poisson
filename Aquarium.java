import java.util.List;
import java.util.ArrayList;

public class Aquarium{
    private int hauteur;
    private int largeur;
    private Dessin dessin;
    private List<PetitPoisson> lesPetitsPoissons;
    private List<GrosPoisson> lesGrosPoissons;
    private List<Algue> lesAlgues;
    private List<Bulle> lesBulles;
    
    public Aquarium(){
        this.hauteur = 50;
        this.largeur = 120;
        this.dessin = new Dessin();
        this.lesPetitsPoissons = new ArrayList<>();
        this.lesGrosPoissons = new ArrayList<>();
        this.lesAlgues = new ArrayList<>();
        this.lesBulles = new ArrayList<>();
    }

    public int getHauteur(){
        return this.hauteur;
    }

    public int getLargeur(){
        return this.largeur;
    }

    public Dessin getDessin(){
        Dessin dessin = new Dessin();
        String couleur = "0x0000F0";
        for (int i=0;i<this.largeur; i++){
            dessin.ajouteChaine(i, 0,  "~", couleur);
        }
        for (int i=0;i<this.largeur; i++){
            dessin.ajouteChaine(i, 49,  "~", couleur);
        }
        return dessin;
    }

    public void evolue(){
        for(GrosPoisson grosPoisson: this.lesGrosPoissons){
            grosPoisson.nage();
        }
        for(PetitPoisson petitPoisson: this.lesPetitsPoissons){
            petitPoisson.nage();
        }
        for(Algue algue: this.lesAlgues){
            algue.ondule();
        }
        for(Bulle bulle: this.lesBulles){
            bulle.remonte();
        }
    }
}